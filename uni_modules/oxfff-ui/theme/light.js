import Vue from 'vue'

module.exports = {
	name: 'light',
	title: '光亮',
	config: {},
	created: function() {
		this.config.color.primary.main = '#2979ff'
		this.config.color.content.main = '#000'
		this.config.color['window-background'] = "rgb(243, 244, 246)"
		this.config.color['window-title-background'] = "rgb(255, 255, 255)"
		this.config.color['popup-background'] = "aliceblue"
		Vue.set(this.config.props, 'ff-cell-group', {
			bgColor: '#ffffff',
			borderColor: 'rgb(239 239 239)',
		})
	},
	unmounted: function() {

	}
}