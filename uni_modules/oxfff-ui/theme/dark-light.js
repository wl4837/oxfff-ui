import Vue from 'vue'

module.exports = {
	name: 'dark-light',
	title: '暗光',
	config: {
		props: {

		},
	},
	callback: null,
	created: function() {
		this.onDarkChange()
		this.callback = ({
			theme
		}) => {
			this.onDarkChange()
		}
		uni.onThemeChange(this.callback)
	},
	unmounted: function() {
		uni.offThemeChange(this.callback)
	},
	onDarkChange() {
		if (uni.getSystemInfoSync().theme == "dark") {
			this.title = '(暗) 暗光'
			this.config.color.primary.main = '#666'
			this.config.color.content.main = '#fff'
			this.config.color['window-background'] = "#000"
			this.config.color['window-title-background'] = "#1C1C1C"
			this.config.color['popup-background'] = "#666"
			Vue.set(this.config.props, 'ff-cell-group', {
				bgColor: '#1C1C1C',
				borderColor: 'rgb(79 79 79)',
			})
		} else {
			this.title = '(光) 暗光'
			this.config.color.primary.main = '#2979ff'
			this.config.color.content.main = '#000'
			this.config.color['window-background'] = "rgb(243, 244, 246)"
			this.config.color['window-title-background'] = "rgb(255 255 255 / 90%)"
			this.config.color['popup-background'] = "aliceblue"
			Vue.set(this.config.props, 'ff-cell-group', {
				bgColor: '#ffffff',
				borderColor: 'rgb(239 239 239)',
			})
		}
	},
}