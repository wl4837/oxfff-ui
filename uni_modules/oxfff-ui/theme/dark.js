import Vue from 'vue'

module.exports = {
	name: 'dark',
	title: '暗黑',
	config: {},
	created: function() {
		this.config.color.primary.main = '#666'
		this.config.color.content.main = '#fff'
		this.config.color['window-background'] = "#000"
		this.config.color['window-title-background'] = "#1C1C1C"
		this.config.color['popup-background'] = "#666"
		Vue.set(this.config.props, 'ff-cell-group', {
			bgColor: '#1C1C1C',
			borderColor: 'rgb(79 79 79)',
		})
	},
	unmounted: function() {

	}
}