module.exports = {
	'default': {
		'main': '#ffffff', // 主要
		'secondary': 'rgb(242, 246, 252)', //次要
		'dark': 'rgb(242, 246, 252)', // 暗
		'light': '#ffffff', // 光
		'disabled': 'rgb(238 238 238)', // 禁用
		'content': { // 文字
			'main': '#000000', // 主要
			'secondary': '#000000', //次要
			'dark': '#000000', // 暗
			'light': '#000000', // 光
			'disabled': '#878484', // 禁用
		},
	},
	'primary': {
		'main': '#409EFF', // 主要
		'secondary': 'rgb(217, 236, 255)', //次要
		'dark': '#2160ca', // 暗
		'light': '', // 光
		'disabled': '#a0cfff', // 禁用
		'content': { // 文字
			'main': '#ffffff', // 主要
			'secondary': '#ffffff', //次要
			'dark': '#ffffff', // 暗
			'light': '#ffffff', // 光
			'disabled': '#ffffff', // 禁用
		},
	},
	'warning': {
		'main': '#E6A23C', // 主要
		'secondary': 'rgb(250, 236, 216)', //次要
		'dark': '', // 暗
		'light': '', // 光
		'disabled': '#f3d19e', // 禁用
		'content': { // 文字
			'main': '#ffffff', // 主要
			'secondary': '#ffffff', //次要
			'dark': '#ffffff', // 暗
			'light': '#ffffff', // 光
			'disabled': '#ffffff', // 禁用
		},
	},
	'success': {
		'main': '#67C23A', // 主要
		'secondary': 'rgb(225, 243, 216)', //次要
		'dark': '', // 暗
		'light': '', // 光
		'disabled': '#b3e19d', // 禁用
		'content': { // 文字
			'main': '#ffffff', // 主要
			'secondary': '#ffffff', //次要
			'dark': '#ffffff', // 暗
			'light': '#ffffff', // 光
			'disabled': '#ffffff', // 禁用
		},
	},
	'danger': {
		'main': '#F56C6C', // 主要
		'secondary': 'rgb(253, 226, 226)', //次要
		'dark': '', // 暗
		'light': '', // 光
		'disabled': '#fab6b6', // 禁用
		'content': { // 文字
			'main': '#ffffff', // 主要
			'secondary': '#ffffff', //次要
			'dark': '#ffffff', // 暗
			'light': '#ffffff', // 光
			'disabled': '#ffffff', // 禁用
		},
	},
	'info': {
		'main': '#909399', // 主要
		'secondary': 'rgb(253, 226, 226)', //次要
		'dark': '', // 暗
		'light': '', // 光
		'disabled': '#c8c9cc', // 禁用
		'content': { // 文字
			'main': '#ffffff', // 主要
			'secondary': '#ffffff', //次要
			'dark': '#ffffff', // 暗
			'light': '#ffffff', // 光
			'disabled': '#ffffff', // 禁用
		},
	},
	'content': {
		'main': '#000000',
		'disabled': '#a0cfff',
		'text': '#ffffff',
		'border': '#000000',
	},
	'background': {
		'dark': '#000000',
		'transparent': 'transparent',
		'light': '#ffffff',
	},
	'mask': 'hsl(0deg 0% 0% / 45%)', // 遮罩层
	'window-background': 'rgb(243, 244, 246)', //窗口背景
	'window-title-background': 'rgb(255 255 255)', //窗口标题背景
	'container-background': '#ffffff', // 容器背景
}