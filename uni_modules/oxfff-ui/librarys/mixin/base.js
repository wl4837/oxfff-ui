// 配置信息
import Vue from 'vue'

module.exports = {
	props: {},
	$ff: {},
	data() {
		return {

		}
	},
	beforeCreate() {
		this.$ff = Vue.observable({})
		this.$set(this.$ff, "id", uni.$ff.util.guid(20))
		this.$set(this.$ff, "uid", this._uid)
		this.$set(this.$ff, "name", this.$options.name)
		this.$set(this.$ff, "class", this.$options.name + '-' + this.$ff.id)
		this.$set(this.$ff, "isRoot", this._uid == this.$root._uid)
		this.$set(this.$ff, "theme", {})
		this.$set(this.$ff, "config", {})
		this.$set(this.$ff, "$props", {})
		this.$set(this.$ff, "$listeners", [])
		this.$set(this.$ff.config, "props", {})
		let updateLock = false
		Object.defineProperty(this.$ff, 'updateLock', {
			get: function() {
				return updateLock
			},
			set: function(val) {
				updateLock = val
			}
		})
		Object.defineProperty(this.$ff, 'moment', {
			get: function() {
				return uni.$ff.moment
			},
		})
		Object.defineProperty(this.$ff, 'util', {
			get: function() {
				return uni.$ff.util
			},
		})
		Object.defineProperty(this.$ff, 'pay', {
			get: function() {
				return uni.$ff.pay
			},
			set: function(val) {
				uni.$ff.pay = val
			}
		})
		Object.defineProperty(this.$ff.config, 'color', {
			get: function() {
				return uni.$ff.config.color
			},
			set: function(val) {
				uni.$ff.config.color = val
			}
		})
		Object.defineProperty(this.$ff.config, 'unit', {
			get: function() {
				return uni.$ff.config.unit
			},
			set: function(val) {
				uni.$ff.config.unit = val
			}
		})
		Object.defineProperty(this.$ff.config, 'style', {
			get: function() {
				return uni.$ff.config.style
			},
			set: function(val) {
				uni.$ff.config.style = val
			}
		})
		Object.defineProperty(this.$ff.config, 'icon', {
			get: function() {
				return uni.$ff.config.icon
			},
			set: function(val) {
				uni.$ff.config.icon = val
			}
		})
		Object.defineProperty(this.$ff, 'color', {
			get: function() {
				return uni.$ff.config.color
			},
			set: function(val) {
				uni.$ff.config.color = val
			}
		})
		Object.defineProperty(this.$ff, 'unit', {
			get: function() {
				return uni.$ff.config.unit
			},
			set: function(val) {
				uni.$ff.config.unit = val
			}
		})
		Object.defineProperty(this.$ff, 'style', {
			get: function() {
				return uni.$ff.config.style
			},
			set: function(val) {
				uni.$ff.config.style = val
			}
		})
		Object.defineProperty(this.$ff.theme, 'active', {
			get: function() {
				return uni.$ff.theme.active
			},
			set: function(val) {
				uni.$ff.theme.active = val
			}
		})
		Object.defineProperty(this.$ff.theme, 'template', {
			get: function() {
				return uni.$ff.theme.template
			},
			set: function(val) {
				uni.$ff.theme.template = val
			}
		})
		this.$set(this.$ff, "isComponent", this.$options.name === "VUniView" || this.$options.name === undefined ||
			this.$options.name === null ?
			false : true)
		this.$set(this.$ff, "component", {})
		this.$set(this.$ff.component, "self", this)
		this.$set(this.$ff.component, "name", this.$options.name)
		this.$set(this.$ff.component, "uid", this._uid)
		this.$set(this.$ff.component, "root", this.$root)
		this.$set(this.$ff.component, "attrs", this.$attrs)
		this.$set(this.$ff.component, "props", this.$props)
		this.$set(this.$ff.component, "parent", this.$parent)
		this.$set(this.$ff.component, "children", this.$children)
		this.$set(this.$ff.component, "bind", {})
		this.$set(this.$ff.component, "bindChildren", {})
		this.$set(this.$ff.component, "isComponent", this.$options.name === "VUniView" ? false : true)
		Object.defineProperty(this.$ff.component, 'all', {
			get: function() {
				return this.$ff.util.getChildrenByList("$children", [this.$root])
			}.bind(this)
		})
		Object.defineProperty(this.$ff.component, 'childrenAll', {
			get: function() {
				return this.$ff.util.getChildrenByList("$children", this.$children)
			}.bind(this)
		})
		Object.defineProperty(this.$ff.component, 'isParent', {
			get: function() {
				if (this.$parent !== undefined && this.$parent !== null && this._uid !== this.$parent
					._uid) {
					return false
				} else {
					return true
				}
			}.bind(this)
		})
		Object.defineProperty(this.$ff.component, 'parentAll', {
			get: function() {
				let list = []
				let parent = this.$parent
				while (parent !== undefined && parent !== null && this._uid !== parent._uid) {
					list.push(parent)
					parent = parent.$parent
				}
				return list
			}.bind(this)
		})
		Object.defineProperty(this.$ff.component, 'allComponent', {
			get: function() {
				let list = []
				for (let i = 0; i < this.$ff.component.all.length; i++) {
					if (this.$ff.component.all[i].$options.name !== "VUniView") {
						list.push(this.$ff.component.all[i])
					}
				}
				return list
			}.bind(this)
		})
		Object.defineProperty(this.$ff.component, 'childrenComponent', {
			get: function() {
				let handleFun = function(children, handle) {
					let list = []
					for (let i = 0; i < children.length; i++) {
						if (children[i].$options.name !== "VUniView") {
							list.push(children[i])
						} else {
							let childrenList = handle(children[i].$children, handle)
							for (let j = 0; j < childrenList.length; j++) {
								list.push(childrenList[j])
							}
						}
					}
					return list
				}
				return handleFun(this.$ff.component.children, handleFun)
			}.bind(this)
		})
		Object.defineProperty(this.$ff.component, 'childrenAllComponent', {
			get: function() {
				let list = []
				for (let i = 0; i < this.$ff.component.childrenAll.length; i++) {
					if (this.$ff.component.childrenAll[i].$options.name !== "VUniView") {
						list.push(this.$ff.component.childrenAll[i])
					}
				}
				return list
			}.bind(this)
		})
		Object.defineProperty(this.$ff.component, 'parentComponent', {
			get: function() {
				let parent = this.$parent
				while (parent !== undefined && parent !== null && this._uid !== parent._uid) {
					if (parent.$options.name !== "VUniView") {
						return parent
					}
					parent = parent.$parent
				}
				return null
			}.bind(this)
		})
		this.$ff.component.finds = (name) => {
			let list = []
			for (let i = 0; i < this.$ff.component.all.length; i++) {
				if (this.$ff.component.all[i].$options.name !== "VUniView") {
					if (this.$ff.component.all[i].$options.name === name) {
						list.push(this.$ff.component.all[i])
					}
				}
			}
			return list
		}
		this.$ff.component.parentFinds = (name) => {
			let list = []
			for (let i = 0; i < this.$ff.component.parentAll.length; i++) {
				if (this.$ff.component.parentAll[i].$options.name !== "VUniView") {
					if (this.$ff.component.parentAll[i].$options.name === name) {
						list.push(this.$ff.component.parentAll[i])
					}
				}
			}
			return list
		}
		this.$ff.component.childrenFinds = (name) => {
			let list = []
			for (let i = 0; i < this.$ff.component.childrenAll.length; i++) {
				if (this.$ff.component.childrenAll[i].$options.name !== "VUniView") {
					if (this.$ff.component.childrenAll[i].$options.name === name) {
						list.push(this.$ff.component.childrenAll[i])
					}
				}
			}
			return list
		}
		this.$ff.component.find = (name) => {
			let list = this.$ff.component.finds(name)
			return list.length > 0 ? list[0] : null
		}
		this.$ff.component.parentFind = (name) => {
			let list = this.$ff.component.parentFinds(name)
			return list.length > 0 ? list[0] : null
		}
		this.$ff.component.childrenFind = (name) => {
			let list = this.$ff.component.childrenFinds(name)
			return list.length > 0 ? list[0] : null
		}
		this.$ff.focus = function() {
			let isFocusComponent = (focusElement) => {
				if (focusElement.$ff.name == "ff-money-keyboard-input") {
					if (this.$root.$ff.focusComponent != null) {
						if (this.$root.$ff.focusComponent.$ff.uid != focusElement.$ff.uid) {
							this.$ff.ufocus()
							this.$root.$ff.focusComponent = focusElement
							focusElement.focus()
						}
					} else {
						this.$root.$ff.focusComponent = focusElement
						focusElement.focus()
					}
					return true
				}
				return false
			}
			if (isFocusComponent(this)) {
				return true
			} else {
				for (let i = 0; i < this.$ff.component.childrenAllComponent.length; i++) {
					if (isFocusComponent(this.$ff.component.childrenAllComponent[i])) {
						return true
					}
				}
			}
			return false
		}.bind(this)
		this.$ff.ufocus = function() {
			if (this.$root.$ff.focusComponent == null) {
				return
			}
			if (this.$root.$ff.focusComponent.$ff.name == "ff-money-keyboard-input") {
				this.$root.$ff.focusComponent.ufocus()
			}
			this.$root.$ff.focusComponent = null
		}.bind(this)
		Object.defineProperty(this.$ff, '$confirm', {
			get: function() {
				for (let i = 0; i < this.$ff.component.all.length; i++) {
					if (this.$ff.component.all[i].$options.name === 'ff-confirm') {
						if (this.$ff.component.all[i].$attrs['ff-name'] !== undefined) {
							if (this.$ff.component.all[i].$attrs['ff-name'] == 'confirm') {
								return this.$ff.component.all[i].open
							}
						}
					}
				}
			}.bind(this)
		})
		Object.defineProperty(this.$ff, '$pay', {
			get: function() {
				for (let i = 0; i < this.$ff.component.all.length; i++) {
					if (this.$ff.component.all[i].$options.name === 'ff-pay') {
						if (this.$ff.component.all[i].$attrs['ff-name'] !== undefined) {
							if (this.$ff.component.all[i].$attrs['ff-name'] == 'pay') {
								return this.$ff.component.all[i].open
							}
						}
					}
				}
			}.bind(this)
		})
		Object.defineProperty(this.$ff, '$toast', {
			get: function() {
				for (let i = 0; i < this.$ff.component.all.length; i++) {
					if (this.$ff.component.all[i].$options.name === 'ff-toast') {
						if (this.$ff.component.all[i].$attrs['ff-name'] !== undefined) {
							if (this.$ff.component.all[i].$attrs['ff-name'] == 'toast') {
								return this.$ff.component.all[i]
							}
						}
					}
				}
			}.bind(this)
		})
		this.$ff.$on = function(eventName, callback) {
			let event = {
				eventName: eventName,
				callback: callback,
				uid: uni.$ff.util.guid(20),
				once: false,
			}
			this.$ff.$listeners.push(event)
			return event
		}.bind(this)
		this.$ff.$off = function(eventName, callback) {
			if (callback != undefined) {
				for (let i = 0; i < this.$ff.$listeners.length; i++) {
					if (this.$ff.$listeners[i].uid == callback.uid) {
						this.$ff.$listeners.splice(i, 1);
					}
				}
			} else {
				for (let i = 0; i < this.$ff.$listeners.length; i++) {
					if (this.$ff.$listeners[i].eventName == eventName) {
						this.$ff.$listeners.splice(i, 1);
					}
				}
			}
		}.bind(this)
		this.$ff.$once = function(eventName, callback) {
			let event = {
				eventName: eventName,
				callback: callback,
				uid: uni.$ff.util.guid(20),
				once: true,
			}
			this.$ff.$listeners.push(event)
			return event
		}.bind(this)
		this.$ff.$emit = function(eventName, data) {
			let retValue = undefined
			for (let i = 0; i < this.$ff.$listeners.length; i++) {
				if (this.$ff.$listeners[i].eventName == eventName) {
					this.$ff.$listeners[i].callback(data);
					if (this.$ff.$listeners[i].once) {
						retValue = this.$ff.$off(this.$ff.$listeners[i].uid)
					}
				}
			}
			return retValue
		}.bind(this)
		this.$ff.component.bind = function() {
			let bind = {}
			let attrKeys = Object.keys(this.$attrs)
			for (let i = 0; i < attrKeys.length; i++) {
				if (attrKeys[i].indexOf("ff-bind:") == 0) {
					bind[attrKeys[i].replace("ff-bind:", "")] = null
				}
			}
			let bindKey = Object.keys(bind)
			if (bindKey.length) {
				for (let i = 0; i < this.$ff.component.all.length; i++) {
					if (this.$ff.component.all[i].$attrs['ff-name'] != undefined) {
						for (let j = 0; j < bindKey.length; j++) {
							if (this.$attrs["ff-bind:" + bindKey[j]] == this.$ff.component.all[i]
								.$attrs[
									'ff-name']) {
								bind[bindKey[j]] = this.$ff.component.all[i]
							}
						}
					}
				}
			}
			return bind
		}.bind(this)()
		this.$ff.component.bindChildren = function() {
			let bindChildren = []
			if (this.$attrs['ff-name'] != undefined) {
				for (let i = 0; i < this.$ff.component.all.length; i++) {
					let attrKeys = Object.keys(this.$ff.component.all[i].$attrs)
					for (let j = 0; j < attrKeys.length; j++) {
						if (attrKeys[j].indexOf("ff-bind:") == 0) {
							if (this.$ff.component.all[i].$attrs[attrKeys[j]] == this.$attrs[
									'ff-name']) {
								bindChildren.push(this.$ff.component.all[i])
							}
						}
					}
				}
			}
			return bindChildren
		}.bind(this)()
	},
	created() {
		if (this.$ff.isRoot) {
			this.$set(this.$ff, "$root", this)
			this.$set(this.$ff, "focusComponent", null)
			uni.$ff.component = this.$ff.component
		}
		this.ffUpdate()
	},
	mounted() {

	},
	updated() {
		if (this.$ff.updateLock == false) {
			this.$ff.updateLock = true;
			this.$nextTick(() => {
				this.ffUpdate()
				this.$nextTick(() => {
					for (let i = 0; i < this.$ff.component.children.length; i++) {
						this.$ff.component.children[i].ffUpdate()
					}
					this.$nextTick(() => {
						this.$ff.updateLock = false
					})
				})
			})
		}
	},
	methods: {
		ffUpdate() {
			let props = {}
			
			this.$ff.$emit("props.before", props)
			
			// 默认值
			for (let i = 0; i < Object.keys(this.$options.props).length; i++) {
				if (Object.prototype.toString.call(
						this.$options.props[Object.keys(this.$options.props)[i]].default) ===
					'[object Function]') {
					props[Object.keys(this.$options.props)[i]] = this.$options.props[
						Object.keys(this.$options.props)[i]].default()
				} else {
					props[Object.keys(this.$options.props)[i]] = this.$options.props[
						Object.keys(this.$options.props)[i]].default
				}
			}
			
			this.$ff.$emit("props.default", props)

			// 全局值
			if (uni.$ff.config.props[this.$ff.name] != undefined) {
				for (let i = 0; i < Object.keys(uni.$ff.config.props[this.$ff.name]).length; i++) {
					props[Object.keys(uni.$ff.config.props[this.$ff.name])[i]] =
						uni.$ff.config.props[this.$ff.name][Object.keys(uni.$ff.config.props[this.$ff
							.name])[
							i]]
				}
			}
			
			this.$ff.$emit("props.global", props)

			// 上级值
			let parentAll = [this.$ff.component.self, ...this.$ff.component.parentAll]
			for (let i = (parentAll.length - 1); i >= 0; i--) {
				if (parentAll[i].$ff.config.props[this.$ff.name] != undefined) {
					for (let j = 0; j < Object.keys(parentAll[i]
							.$ff.config.props[this.$ff.name]).length; j++) {
						props[Object.keys(parentAll[i].$ff.config.props[this.$ff.name])[
								j]] =
							parentAll[i].$ff.config.props[this.$ff.name]
							[Object.keys(parentAll[i].$ff.config.props[this.$ff.name])[j]]
					}
				}
			}
			
			this.$ff.$emit("props.parent", props)

			// 组件值
			if (this.$options.propsData != undefined) {
				for (let i = 0; i < Object.keys(this.$options.propsData).length; i++) {
					props[Object.keys(this.$options.propsData)[i]] =
						this.$options.propsData[Object.keys(this.$options.propsData)[i]]
				}
			}
			
			this.$ff.$emit("props.component", props)

			this.$ff.$emit("props.after", props)
			
			this.$set(this.$ff, "$props", props)
		},
	},
}