import { deepCopy } from './librarys/util'

const install = (Vue) => {
	uni.$ff = Vue.observable({
		util: require('./librarys/util'),
		config: {
			icon: require('./librarys/config/icon'),
		},
		pay: {
			list: [],
		},
		theme: {
			_active: {},
			template: {
				'primary': require('./theme/primary'),
				'dark-light': require('./theme/dark-light'),
				'dark': require('./theme/dark'),
				'light': require('./theme/light'),
			},
		},
		component: {}
	})
	Object.defineProperty(uni.$ff.theme, 'active', {
		get: function() {
			return uni.$ff.theme._active
		},
		set: function(val) {
			if (val.name != uni.$ff.theme._active.name) {
				if (uni.$ff.theme._active != null) {
					if (uni.$ff.theme._active.unmounted != undefined) {
						uni.$ff.theme._active.unmounted()
					}
				}
				let activeTheme = uni.$ff.util.deepCopy(val)
				activeTheme.config = activeTheme.config == undefined ? {} : activeTheme.config
				activeTheme.config.color = uni.$ff.util.deepAssignCopy({}, uni.$ff.util.deepCopy(require('./librarys/config/color')),
					activeTheme.config.color == undefined ? {} : activeTheme.config.color)
				activeTheme.config.props = uni.$ff.util.deepAssignCopy({}, uni.$ff.util.deepCopy(require('./librarys/config/props')),
					activeTheme.config.props == undefined ? {} : activeTheme.config.props)
				activeTheme.config.style = uni.$ff.util.deepAssignCopy({}, uni.$ff.util.deepCopy(require('./librarys/config/style')),
					activeTheme.config.style == undefined ? {} : activeTheme.config.style)
				activeTheme.config.unit = activeTheme.config.unit == undefined ? 'px' : activeTheme.config.unit
				uni.$ff.theme._active = Vue.observable(activeTheme)
				uni.$ff.theme._active.created()
			}
		}
	})
	Object.defineProperty(uni.$ff.config, 'color', {
		get: function() {
			return uni.$ff.theme.active.config.color
		},
		set: function(val) {
			uni.$ff.theme.active.config.color = val
		}
	})
	Object.defineProperty(uni.$ff.config, 'unit', {
		get: function() {
			return uni.$ff.theme.active.config.unit
		},
		set: function(val) {
			uni.$ff.theme.active.config.unit = val
		}
	})
	Object.defineProperty(uni.$ff.config, 'style', {
		get: function() {
			return uni.$ff.theme.active.config.style
		},
		set: function(val) {
			uni.$ff.theme.active.config.style = val
		}
	})
	Object.defineProperty(uni.$ff.config, 'props', {
		get: function() {
			return uni.$ff.theme.active.config.props
		},
		set: function(val) {
			uni.$ff.theme.active.config.props = val
		}
	})
	Object.defineProperty(uni.$ff, 'color', {
		get: function() {
			return uni.$ff.config.color
		},
		set: function(val) {
			uni.$ff.config.color = val
		}
	})
	Object.defineProperty(uni.$ff, 'unit', {
		get: function() {
			return uni.$ff.config.unit
		},
		set: function(val) {
			uni.$ff.config.unit = val
		}
	})
	Object.defineProperty(uni.$ff, 'style', {
		get: function() {
			return uni.$ff.config.style
		},
		set: function(val) {
			uni.$ff.config.style = val
		}
	})
	Object.defineProperty(uni.$ff, 'props', {
		get: function() {
			return uni.$ff.config.props
		},
		set: function(val) {
			uni.$ff.config.props = val
		}
	})
	uni.$ff.theme.active = uni.$ff.theme.template.primary
	Vue.mixin(require('./librarys/mixin/base'))
}

export default {
	install
}