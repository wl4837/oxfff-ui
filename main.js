import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'

import uView from 'uni_modules/uview-ui'
Vue.use(uView)

import oxfff from 'uni_modules/oxfff-ui'
Vue.use(oxfff)

uni.$ff.theme.active = uni.$ff.theme.template['dark-light']

uni.$ff.pay.list = [{
		name: '微信支付',
		platform: ['h5'],
		payment: 'wxpay',
	},
	{
		name: '支付宝支付',
		platform: ['h5'],
		payment: 'alipay',
	},
]

uni.$ff.config.icon.home = {
	label: '首页',
	name: 'icon-shouye',
	fontFamily: 'iconfont',
}

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif